//
//  RoomView.swift
//  OpenTokDemo
//
//  Created by aprillee on 2022/1/4.
//

import SwiftUI

struct RoomView: View {
    
    @StateObject var viewModel = OpenTokViewModel()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        ZStack {
            Rectangle()
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .foregroundColor(.white)
                .ignoresSafeArea(.container, edges: .all)
            
            VStack(spacing: 5) {
                HStack {
                    Spacer()
                    Button {
                        viewModel.disconnectSession()
                    } label: {
                        Text("Leave")
                            .foregroundColor(.red)
                            .font(.system(size: 24))
                            .padding(.horizontal, 5)
                            .padding(.vertical, 6)
                    }
                    .overlay(RoundedRectangle(cornerRadius: 10).strokeBorder(.red, lineWidth: 2))
                    .padding(.horizontal, 10)
                }

                OpenTokView(viewModel: viewModel)
            }
            
            if viewModel.isloading {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .green))
                    .scaleEffect(2)
            }
        }
        .navigationBarHidden(true)
        .onReceive(viewModel.$disconnect) { disconnect in
            if disconnect {
                presentationMode.wrappedValue.dismiss()
            }
        }
        
    }
}

struct RoomView_Previews: PreviewProvider {
    static var previews: some View {
        RoomView()
    }
}
