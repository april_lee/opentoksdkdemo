//
//  MainView.swift
//  OpenTokDemo
//
//  Created by aprillee on 2022/1/4.
//

import SwiftUI

struct MainView: View {
    
    var body: some View {
        ZStack {
            Image("space-gbaa")
                .fixedSize()
            
            NavigationLink {
                RoomView()
            } label: {
                Text("Join")
                    .font(.system(size: 30))
                    .foregroundColor(.black)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 3).stroke(.purple, lineWidth: 3))
            }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
