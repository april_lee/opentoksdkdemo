//
//  OpenTokView.swift
//  OpenTokDemo
//
//  Created by aprillee on 2022/1/4.
//

import SwiftUI
import OpenTok

struct OpenTokView: View {
    
    @StateObject var viewModel: OpenTokViewModel
    
    private let screenWidth = UIScreen.main.bounds.width

    @State var audio = true
    @State var subscribers: [OTSubscriber] = []
    
    var body: some View {
        ZStack {
            PublisherView(viewModel: viewModel)
            VStack {
                HStack {
                    if viewModel.subscribers.count > 0 {
                        SubscriberView(subscriber: viewModel.subscribers[0])
                    }
                    Spacer()
                    if viewModel.subscribers.count > 1 {
                        SubscriberView(subscriber: viewModel.subscribers[1])
                    }
                }
                Spacer()
                HStack {
                    if viewModel.subscribers.count > 2 {
                        SubscriberView(subscriber: viewModel.subscribers[2])
                    }
                    Spacer()
                    if viewModel.subscribers.count > 3 {
                        SubscriberView(subscriber: viewModel.subscribers[3])
                    }
                }
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
}

struct PublisherView: View {
    
    @StateObject var viewModel: OpenTokViewModel
    
    @State var enabledAuto: Bool = true
    @State var enabledVideo: Bool = true
    
    private let screenWidth = UIScreen.main.bounds.width
    private let screenHeight = UIScreen.main.bounds.height
    
    var body: some View {
        
        if let view = viewModel.publisher.publisher?.view {
            OTView(view: view)
                .overlay(
                    VStack {
                        Text("Me")
                            .foregroundColor(.green)
                            .font(.system(size: 20))
                        ToolView(enableAuto: $enabledAuto, enableVideo: $enabledVideo)
                    }
                    , alignment: .top
                )
                .onChange(of: enabledAuto) { enableAuto in
                    viewModel.publisher.enabledAuto = enableAuto
                }
                .onChange(of: enabledVideo) { enableVideo in
                    viewModel.publisher.enabledVideo = enableVideo
                }
        }
    }
}

struct SubscriberView: View {
    
    @State var subscriber: OTokSubscriber
    
    @State var enabledAuto: Bool = true
    @State var enabledVideo: Bool = true
    
    private let screenWidth = UIScreen.main.bounds.width
    
    var body: some View {
        
        if let view = subscriber.subscriber.view {
            OTView(view: view)
                .frame(width: 150 , height: 150)
                .overlay(
                    ToolView(enableAuto: $enabledAuto, enableVideo: $enabledVideo)
                , alignment: .topLeading)
                .onChange(of: enabledAuto) { enableAuto in
                    subscriber.enabledAuto = enableAuto
                }
                .onChange(of: enabledVideo) { enableVideo in
                    subscriber.enabledVideo = enableVideo
                }
        }
    }
}




struct ToolView: View {
    
    @Binding var enableAuto: Bool
    @Binding var enableVideo: Bool
    
    var body: some View {
        HStack {
            Button {
                enableAuto.toggle()
            } label: {
                Image(systemName: enableAuto ? "speaker.wave.3.fill" : "speaker.slash.fill")
                    .foregroundColor(.white)
                    .frame(width: 40, height: 40)
            }
            
            Button {
                enableVideo.toggle()
            } label: {
                Image(systemName: enableVideo ? "video.fill" : "video.slash.fill")
                    .foregroundColor(.white)
                    .frame(width: 40, height: 40)
            }
        }
    }
}



struct OpenTokView_Previews: PreviewProvider {
    static var previews: some View {
        OpenTokView(viewModel: OpenTokViewModel())
    }
}
