//
//  ContentView.swift
//  OpenTokSDKDemo
//
//  Created by aprillee on 2022/1/4.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            MainView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
