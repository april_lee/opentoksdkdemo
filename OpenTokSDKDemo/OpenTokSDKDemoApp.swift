//
//  OpenTokSDKDemoApp.swift
//  OpenTokSDKDemo
//
//  Created by aprillee on 2022/1/4.
//

import SwiftUI

@main
struct OpenTokSDKDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
