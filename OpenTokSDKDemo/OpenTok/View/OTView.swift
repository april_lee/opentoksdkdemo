//
//  OTView.swift
//  OpenTokSDKDemo
//
//  Created by aprillee on 2022/1/4.
//

import SwiftUI
import OpenTok

struct OTView: UIViewRepresentable {
    @State var view: UIView
    
    func makeUIView(context: Context) -> UIView {
        return view
    }
    
    func updateUIView(_ uiView: UIView, context: Context) {
        DispatchQueue.main.async {
            self.view = uiView
        }
    }
}



