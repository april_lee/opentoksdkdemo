//
//  OpenTokViewModel.swift
//  OpenTokDemo
//
//  Created by aprillee on 2022/1/3.
//

import Foundation
import Combine
import OpenTok

class OpenTokViewModel: NSObject, ObservableObject {
    
    @Published var disconnect: Bool = false
    @Published var isloading: Bool = true
    
    @Published var publisher: OTokPublisher = OTokPublisher(publisher: nil, enabledAuto: true, enabledVideo: true)
    @Published var subscribers: [OTokSubscriber] = []

    private let apiKey = ""
    private let sessionId = ""
    
    private var session: OTSession?
    
    private(set) var token = ""
    
    
    override init() {
        super.init()
        connectToSession()
    }
    
    // MARK: - Session
    private func connectToSession() {
        
        session = OTSession(apiKey: apiKey, sessionId: sessionId, delegate: self)
        
        var error: OTError?
        session?.connect(withToken: token, error: &error)
        
        if let error = error {
            print("An error occurred connecting to the session, \(error)")
        }
    }
    
    func disconnectSession() {
        var error: OTError?
        session?.disconnect(&error)
        
        if let error = error {
          print("An error occurred disconnecting from the session, \(error)")
        }
    }

    // MARK: - Publisher
    
    private func createPublisher() {
        
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        
        guard let publisher = OTPublisher(delegate: self, settings: settings) else {
            return
        }
        
        self.publisher = OTokPublisher(publisher: publisher, enabledAuto: true, enabledVideo: true)
        
        var error: OTError?
        session?.publish(publisher, error: &error)
        
        if let error = error {
            print("An error occurred when trying to publish, \(error)")
        }
    }
    
    //MARK: Subscriber
    
    private func subscribe(to stream: OTStream) {
        
        guard let subscriber = OTSubscriber(stream: stream, delegate: self) else {
            return
        }
        
        var error: OTError?
        session?.subscribe(subscriber, error: &error)
        
        if let error = error {
            print("An error occurred when subscribing to the stream, \(error)")
            return
        }
        
        subscribers.append(OTokSubscriber(subscriber: subscriber, enabledAuto: true, enabledVideo: true))
    }
}

//MARK: - Session
extension OpenTokViewModel: OTSessionDelegate {
    
    func sessionDidConnect(_ session: OTSession) {
        print("The client connected to the session.")
        createPublisher()
        
        isloading = false
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("The client disconnected from the session.")
        subscribers.removeAll()
        disconnect = true
    }
    
    //An asynchronous error occurs while trying to connect to the session.
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("The client fail to connect to the session: \(error).")
    }
    
    //Someone else publishes their video stream to the session.
    //This provides an OTStream, which represents their stream.
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("A stream was created in the session")
        
//        if subscribers.count == 4 {
//            print("Sorry this sample only supports up to 4 subscribers :)")
//            return
//        }
//
        subscribe(to: stream)
    }
    
    //Someone else stops publishing their video stream to the session.
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("\(stream.streamId) stream was destroyed in the session")
        
        guard let index = subscribers.firstIndex(where: { $0.subscriber.stream?.streamId == stream.streamId }) else {
            return
        }
        
        subscribers[index].subscriber.view?.removeFromSuperview()
        subscribers.remove(at: index)
    }
}


// MARK: - Publisher

extension OpenTokViewModel: OTPublisherDelegate {
  
    func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
        print("publisher stream created")
    }
    
    func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
        print("publisher stream destroyed")
    }
    
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("Publisher failed: \(error.localizedDescription)")
    }
}

// MARK: - Subscriber

extension OpenTokViewModel: OTSubscriberDelegate {
    func subscriberDidConnect(toStream subscriberKit: OTSubscriberKit) {
        print("Subscriber connected")
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("Subscriber failed: \(error.localizedDescription)")
    }
    
    func subscriberVideoDataReceived(_ subscriber: OTSubscriber) {
    }
}
