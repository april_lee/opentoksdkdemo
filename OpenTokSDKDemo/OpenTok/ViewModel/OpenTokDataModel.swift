//
//  OpenTokDataModel.swift
//  OpenTokSDKDemo
//
//  Created by aprillee on 2022/1/5.
//

import Foundation
import OpenTok

struct OTokPublisher {
    
    let publisher: OTPublisher?
    
    var enabledAuto: Bool {
        didSet {
            publisher?.publishAudio = enabledAuto
        }
    }
    
    var enabledVideo: Bool {
        didSet {
            publisher?.publishVideo = enabledVideo
        }
    }
}

struct OTokSubscriber: Identifiable {
    
    let id = UUID()
    var subscriber: OTSubscriber
    
    var enabledAuto: Bool {
        didSet {
            subscriber.subscribeToAudio = enabledAuto
        }
    }
    
    var enabledVideo: Bool {
        didSet {
            subscriber.subscribeToVideo = enabledVideo
        }
    }
}
