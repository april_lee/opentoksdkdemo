# OpenTok Demo
可以進行1對多人影像通訊
使用 OpenTok
Demo code 使用 UIKit 

## Start
* 於 Vonage’s 註冊帳號[Sign up for the Vonage Video API](https://tokbox.com/account/user/signup?icid=tryitfree_comm-apis_tokboxfreetrialsignup_nav)
    * 免費測用 帳號有 10元美金的使用額度
    * 於Vonage’s後台介面建立 Project，取得 `apiKey`  、`sessionId`  與 `token`
    * 參考 [Vonage Video API: Real-Time Video in iOS | raywenderlich.com](https://www.raywenderlich.com/20935718-vonage-video-api-real-time-video-in-ios)
* 用  [CocoaPods](http://cocoapods.org/)  下載SDK
    * `pod ‘OpenTok’`
* Xcode 使用
    * `import OpenTok`

## Project 
* OpenTok
    * View
        * OTView ( 專用SwiftUI View)
    * ViewModel
        * OpenTokViewModel 
            * apiKey, sessionId, token
            * Connect / Disconnect  Session
            * Create Publisher
            * Subscribe to stream
        * OpenTokDataModel (為了同步畫面用的)
            * OTokPublisher
            * OTokSubscriber
* MainView
* RoomView
    * leave session
* OpenTokView
    * stream list

##  Reference
* [Set up a simple iOS client - Swift | Vonage Video API Developer](https://tokbox.com/developer/tutorials/ios/swift/basic-video-chat/)
* [Sample applications using the OpenTok iOS SDK in Swift](https://github.com/opentok/opentok-ios-sdk-samples-swift)
* [Vonage Video API: Real-Time Video in iOS | raywenderlich.com](https://www.raywenderlich.com/20935718-vonage-video-api-real-time-video-in-ios)
* [SDK Reference](https://tokbox.com/developer/sdks/ios/reference/)